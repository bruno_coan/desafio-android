package br.com.exemplo.cep.service;

import retrofit.ErrorHandler;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class ServiceErrorHandler implements ErrorHandler {

	@Override
	public Throwable handleError(RetrofitError cause) {
		
		Response response = cause.getResponse();
		if(response != null && response.getStatus() == 404) {
			return new Exception("Cep inválido");
		} else {
			return new Exception("Erro ao consultar CEP");
		}
	}

}
