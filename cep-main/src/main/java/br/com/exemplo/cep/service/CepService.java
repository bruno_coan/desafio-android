package br.com.exemplo.cep.service;

import org.androidannotations.annotations.Background;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;
import org.androidannotations.annotations.UiThread;

import retrofit.RestAdapter;
import android.app.ProgressDialog;
import android.widget.Toast;
import br.com.exemplo.cep.MainActivity;
import br.com.exemplo.cep.domain.Endereco;
import br.com.exemplo.cep.rest.RestClientService;

@EBean
public class CepService {
	
	@RootContext
	MainActivity activity;
	private ProgressDialog progress;
	
	
	
	@Background
	public void execute(String cep) {
		showDialog();
		RestAdapter restAdapter = new RestAdapter.Builder().setEndpoint("http://correiosapi.apphb.com/cep/")
				.setErrorHandler(new ServiceErrorHandler())
				.build();

		RestClientService service = restAdapter.create(RestClientService.class);
		try {
			Endereco endereco = service.buscaCep(cep);
			updateView(endereco);
		} catch(Exception e) {
			showErrorMessage(e.getCause().getMessage());
		} finally {
			progress.dismiss();
		}
	}
	
	@UiThread
	void updateView(Endereco endereco) {
		activity.updateView(endereco);
	}
	
	@UiThread
	void showErrorMessage(String message) {
		activity.showErrorMessage(message);
	}
	@UiThread
	void showDialog() {
		progress = ProgressDialog.show(activity,"Buscando CEP","aguarde enquanto consutamos o endereço" ,true,false);

	}


}
