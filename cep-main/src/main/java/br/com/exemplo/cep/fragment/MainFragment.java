package br.com.exemplo.cep.fragment;

import org.androidannotations.annotations.AfterViews;
import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.Click;
import org.androidannotations.annotations.EFragment;
import org.androidannotations.annotations.ViewById;

import android.app.Fragment;
import android.widget.EditText;
import br.com.exemplo.cep.R;
import br.com.exemplo.cep.dialog.HistoryDialog;
import br.com.exemplo.cep.mask.Mask;
import br.com.exemplo.cep.service.CepService;

@EFragment(R.layout.fragment_main)
public class MainFragment extends Fragment {
	@ViewById
	public EditText txtCep;

	@Bean
	public CepService service;
	
	@Bean
	HistoryDialog dialog;
	
	@AfterViews
	public void setUp() {
		txtCep.addTextChangedListener(Mask.insert("#####-###", txtCep));
	}
	
	@Click
	public void btnBusca() {
		String cep = txtCep.getText().toString();
		service.execute(cep.replace("-", ""));
	}
	@Click
	public void btnHistory() {
		dialog.show(getFragmentManager(), getString(R.string.historico));
	}
	
}
