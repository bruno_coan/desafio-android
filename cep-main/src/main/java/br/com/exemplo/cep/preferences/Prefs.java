package br.com.exemplo.cep.preferences;

import java.util.List;

import org.androidannotations.annotations.sharedpreferences.SharedPref;

import br.com.exemplo.cep.domain.Endereco;
@SharedPref
public interface Prefs {
	
	String history();

}
