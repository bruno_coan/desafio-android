package br.com.exemplo.cep.dialog;

import org.androidannotations.annotations.EBean;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import br.com.exemplo.cep.R;
import br.com.exemplo.cep.domain.Endereco;
@EBean
public class CepDialog extends DialogFragment {
	
	Endereco endereco;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		 View view = getActivity().getLayoutInflater().inflate(R.layout.cep_dialog, null);	 
		 
		 TextView txvCep = (TextView) view.findViewById(R.id.txvCep);
		 TextView txvAddress = (TextView) view.findViewById(R.id.txvAddress);
		 
		txvCep.setText(endereco.getCep());	
		txvAddress.setText(endereco.enderecoCompleto());
		 
		 builder.setView(view)           
		           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		            	   CepDialog.this.getDialog().cancel();
		               }
		           });
		   return builder.create();
	}
	
	public void setEndereco(Endereco endereco) {
		this.endereco = endereco;
	}

}
