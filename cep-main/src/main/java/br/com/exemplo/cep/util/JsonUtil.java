package br.com.exemplo.cep.util;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import br.com.exemplo.cep.domain.Endereco;

public class JsonUtil {
	
	private static Gson gson = new Gson();
	private static Type type = new TypeToken<List<Endereco>>(){}.getType();
	
	public static List<Endereco> fromJson(String json) {
		
		List<Endereco> historyList = new ArrayList<Endereco>();
		
		if(json != null && ! json.equals("")) {
		    historyList = gson.fromJson(json, type);
		}  
		return historyList;
		
	}
	
	public static String toJson(List<Endereco> history) {
		return gson.toJson(history, type);
	}

}
