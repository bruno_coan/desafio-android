package br.com.exemplo.cep.adapter;

import org.androidannotations.annotations.EViewGroup;
import org.androidannotations.annotations.ViewById;

import br.com.exemplo.cep.R;
import br.com.exemplo.cep.domain.Endereco;
import android.content.Context;
import android.widget.LinearLayout;
import android.widget.TextView;

@EViewGroup(R.layout.history_listview)
public class HistoryItemView extends LinearLayout {
	@ViewById
	public TextView txvCep;
	@ViewById
	public TextView txvAddress;
	
	public HistoryItemView(Context context) {
		super(context);
	}
	
	public void bind(Endereco endereco) {
		txvCep.setText(endereco.getCep());	
		txvAddress.setText(endereco.enderecoCompleto());
	}

}
