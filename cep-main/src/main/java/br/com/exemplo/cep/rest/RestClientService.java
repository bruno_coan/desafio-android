package br.com.exemplo.cep.rest;

import br.com.exemplo.cep.domain.Endereco;
import retrofit.http.GET;
import retrofit.http.Path;

public interface RestClientService {
	@GET("/{cep}")
	public Endereco buscaCep(@Path("cep") String cep);

}
