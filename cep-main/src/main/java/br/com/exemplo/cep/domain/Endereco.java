package br.com.exemplo.cep.domain;

import java.io.Serializable;

import br.com.exemplo.cep.mask.Mask;

public class Endereco implements Serializable {
	private String cep;
	private String tipoDeLogradouro;
	private String logradouro;
	private String bairro;
	private String cidade;
	private String estado;
	
	public String getCep() {	
		return Mask.putMask("#####-###", cep, "");
	}
	public void setCep(String cep) {
		this.cep = cep;
	}
	public String getTipoDeLogradouro() {
		return tipoDeLogradouro;
	}
	public void setTipoDeLogradouro(String tipoDeLogradouro) {
		this.tipoDeLogradouro = tipoDeLogradouro;
	}
	public String getLogradouro() {
		return logradouro;
	}
	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}
	public String getBairro() {
		return bairro;
	}
	public void setBairro(String bairro) {
		this.bairro = bairro;
	}
	public String getCidade() {
		return cidade;
	}
	public void setCidade(String cidade) {
		this.cidade = cidade;
	}
	public String getEstado() {
		return estado;
	}
	public void setEstado(String estado) {
		this.estado = estado;
	}
	
	public String enderecoCompleto() {
		StringBuilder address = new StringBuilder(getTipoDeLogradouro());
		address.append(", ").append(getLogradouro()).append(", ").append(getBairro())
		.append(", ").append(getCidade()).append(", ").append(getEstado());
		
		return address.toString();
		
	}
}
