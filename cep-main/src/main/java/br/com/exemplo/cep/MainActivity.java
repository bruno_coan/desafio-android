package br.com.exemplo.cep;

import java.util.List;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EActivity;
import org.androidannotations.annotations.FragmentById;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.Activity;
import br.com.exemplo.cep.dialog.CepDialog;
import br.com.exemplo.cep.dialog.ErrorDialog;
import br.com.exemplo.cep.domain.Endereco;
import br.com.exemplo.cep.fragment.MainFragment;
import br.com.exemplo.cep.preferences.Prefs_;
import br.com.exemplo.cep.util.JsonUtil;

@EActivity(R.layout.activity_main)
public class MainActivity extends Activity {
	
	@FragmentById(R.id.Main_fragment)
	MainFragment fragment;
	
	@Bean
	CepDialog dialog;
	
	@Pref
	public Prefs_ prefs;

	public void updateView(Endereco endereco) {
		
		List<Endereco> historyList = JsonUtil.fromJson(prefs.history().get());
		historyList.add(endereco);
		prefs.history().put(JsonUtil.toJson(historyList));
		
		dialog.setEndereco(endereco);
		dialog.show(getFragmentManager(), getString(R.string.historico));

	}
	
	public void showErrorMessage(String message) {
		
		ErrorDialog errorDialog = new ErrorDialog(message);
		errorDialog.show(getFragmentManager(), null);	
	}



}
