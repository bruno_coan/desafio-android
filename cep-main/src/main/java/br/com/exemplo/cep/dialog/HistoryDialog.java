package br.com.exemplo.cep.dialog;

import org.androidannotations.annotations.Bean;
import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.sharedpreferences.Pref;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import br.com.exemplo.cep.R;
import br.com.exemplo.cep.adapter.HistoryBaseAdapter;
import br.com.exemplo.cep.preferences.Prefs_;
import br.com.exemplo.cep.util.JsonUtil;
@EBean
public class HistoryDialog extends DialogFragment {
	
	@Bean
	HistoryBaseAdapter adapter;
	
	@Pref
	public Prefs_ prefs;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		 AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		 View view = getActivity().getLayoutInflater().inflate(R.layout.history_dialog, null);

		 ListView listView = (ListView) view.findViewById(R.id.lvHistory);
		 
		 adapter.settEnderecos(JsonUtil.fromJson(prefs.history().get()));
		 
		 listView.setAdapter(adapter);
		 
		    builder.setView(view)           
		           .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
		               public void onClick(DialogInterface dialog, int id) {
		            	   HistoryDialog.this.getDialog().cancel();
		               }
		           });
		   return builder.create();
	}
	


}
