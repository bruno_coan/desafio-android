package br.com.exemplo.cep.adapter;

import java.util.List;

import org.androidannotations.annotations.EBean;
import org.androidannotations.annotations.RootContext;

import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import br.com.exemplo.cep.MainActivity;
import br.com.exemplo.cep.domain.Endereco;
@EBean
public class HistoryBaseAdapter extends BaseAdapter {
	
	private List<Endereco> list;
	@RootContext
	MainActivity activity;

	@Override
	public int getCount() {
		return list.size();
	}

	@Override
	public Object getItem(int position) {
		return list.get(position);
	}

	@Override
	public long getItemId(int position) {
		return 0;
	}

	@Override
	public View getView(int position, View view, ViewGroup arg2) {
		
		HistoryItemView historyItemView;
		
		if(view == null ){
			historyItemView = HistoryItemView_.build(activity);
		}else{
			historyItemView = (HistoryItemView) view;
		}
		
		historyItemView.bind(list.get(position));
		
		return historyItemView;
	}
	
	public void settEnderecos(List<Endereco> enderecos) {
		list = enderecos;
	}

}
