//
// DO NOT EDIT THIS FILE, IT HAS BEEN GENERATED USING AndroidAnnotations 3.0.1.
//


package br.com.exemplo.cep.preferences;

import android.content.Context;
import android.content.SharedPreferences;
import org.androidannotations.api.sharedpreferences.EditorHelper;
import org.androidannotations.api.sharedpreferences.SharedPreferencesHelper;
import org.androidannotations.api.sharedpreferences.StringPrefEditorField;
import org.androidannotations.api.sharedpreferences.StringPrefField;

public final class Prefs_
    extends SharedPreferencesHelper
{

    private Context context_;

    public Prefs_(Context context) {
        super(context.getSharedPreferences((getLocalClassName(context)+"_Prefs"), 0));
        this.context_ = context;
    }

    public Prefs_.PrefsEditor_ edit() {
        return new Prefs_.PrefsEditor_(getSharedPreferences());
    }

    private static String getLocalClassName(Context context) {
        String packageName = context.getPackageName();
        String className = context.getClass().getName();
        int packageLen = packageName.length();
        if (((!className.startsWith(packageName))||(className.length()<= packageLen))||(className.charAt(packageLen)!= '.')) {
            return className;
        }
        return className.substring((packageLen + 1));
    }

    public StringPrefField history() {
        return stringField("history", "");
    }

    public final static class PrefsEditor_
        extends EditorHelper<Prefs_.PrefsEditor_>
    {


        PrefsEditor_(SharedPreferences sharedPreferences) {
            super(sharedPreferences);
        }

        public StringPrefEditorField<Prefs_.PrefsEditor_> history() {
            return stringField("history");
        }

    }

}
