package br.com.example.cep.test;

import android.app.Instrumentation;
import android.test.ActivityInstrumentationTestCase2;
import br.com.exemplo.cep.MainActivity;
import br.com.exemplo.cep.R;

public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity> {
	
	MainActivity activity;
	Instrumentation instrumentation;
	
	public MainActivityTest() {
		super(MainActivity.class);
	}
	
	 @Override
	  protected void setUp() throws Exception {
	    super.setUp();
	    setActivityInitialTouchMode(true);
	    activity = getActivity();
	    instrumentation = getInstrumentation();
	  }
	 
	 public void testLayout() {
		 assertNotNull(activity.findViewById(R.id.txtCep));
		 assertNotNull(activity.findViewById(R.id.btnBusca));
		 assertNotNull(activity.findViewById(R.id.btnHistory));

	 }

}
